﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    public class Employee
    {
        private int idEmployee { get; set; }
        private string nameEmployee { get; set; }
        private string lastNameEmployee { get; set; }

        //En agregacion:no hago el new, ya viene desde afuera resuelto
        Manager EmpManager;

        public Employee(Manager emanager)
        {
            EmpManager = emanager;
        }

        public Employee()
        {
            
        }

        public Employee(int idEmployee, string nameEmployee, string lastNameEmployee)
        {
            this.idEmployee = idEmployee;
            this.nameEmployee = nameEmployee;
            this.lastNameEmployee = lastNameEmployee;
        }


    }
}
