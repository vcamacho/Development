﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    class Card
    {
        private int idCard { get; set; }
        private int codigo { get; set; }

        public Card(int idCard, int codigo)
        {
            this.idCard = idCard;
            this.codigo = codigo;
        }
        public void DisplayCard()
        {
            Console.WriteLine("codigo:" + codigo);
        }
     

    }
}
