﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    class Project
    {
        //Relacion de composicion: hago el new dentro la clase
        Manager manager = new Manager(10);


        //Atributos de la clase
        private int idProject { get; set; }
        private string name { get; set; }

        //Constructor
        public Project(int idProject, string name)
        {
            this.idProject = idProject;
            this.name = name;
        }


   }
}
